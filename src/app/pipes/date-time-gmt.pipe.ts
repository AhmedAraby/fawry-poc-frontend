import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateTimeGmt'
})
export class dateTimeGmt implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    return new Date(value).toUTCString();  // this is behind us with 2 hours
  }

}
