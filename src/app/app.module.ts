import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { PolicyFormComponent } from './components/reference-number-payment/policy-form/policy-form.component';
import { ReferenceNumberPaymentResultComponent } from './components/reference-number-payment/reference-number-payment-result/reference-number-payment-result.component';
import { ReferenceNumberPaymentComponent } from './components/reference-number-payment/reference-number-payment.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { dateTimeGmt } from './pipes/date-time-gmt.pipe';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    PolicyFormComponent,
    ReferenceNumberPaymentResultComponent,
    ReferenceNumberPaymentComponent,
    dateTimeGmt, 
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,  // to be able to use the construct defined in this module in templates
    RouterModule,   // router-outlet need it
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
