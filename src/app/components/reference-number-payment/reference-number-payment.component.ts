import { Component, OnDestroy, OnInit } from '@angular/core';
import { FawryReferenceNumberPaymentResponseModel } from 'src/app/models/fawry-reference-number-response.model';
import { ReferenceNumberPaymentService } from 'src/app/services/fawry/reference-number-payment.service';

@Component({
  selector: 'app-reference-number-payment',
  templateUrl: './reference-number-payment.component.html',
  styleUrls: ['./reference-number-payment.component.css']
})
export class ReferenceNumberPaymentComponent implements OnInit, OnDestroy{

  refNumResp = null;
  refNumRespSub = null;

  constructor(
    private refNumPaymentService: ReferenceNumberPaymentService
    )
  { 
    this.refNumRespSub = this.refNumPaymentService.refNumRespSubject.subscribe(
      (refNumResp: FawryReferenceNumberPaymentResponseModel)=>{
        this.refNumResp = refNumResp;
      }
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy()
  {
    if(this.refNumRespSub)
      this.refNumRespSub.unsubscribe();
  }

}
