import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceNumberPaymentComponent } from './reference-number-payment.component';

describe('ReferenceNumberPaymentComponent', () => {
  let component: ReferenceNumberPaymentComponent;
  let fixture: ComponentFixture<ReferenceNumberPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferenceNumberPaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceNumberPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
