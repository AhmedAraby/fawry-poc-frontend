import { Component, Input, OnInit } from '@angular/core';
import { FawryReferenceNumberPaymentResponseModel } from 'src/app/models/fawry-reference-number-response.model';

@Component({
  selector: 'app-reference-number-payment-result',
  templateUrl: './reference-number-payment-result.component.html',
  styleUrls: ['./reference-number-payment-result.component.css']
})
export class ReferenceNumberPaymentResultComponent implements OnInit {

  @Input() refNumResp:FawryReferenceNumberPaymentResponseModel = null;
  constructor() { }

  ngOnInit(): void {
  }

}
