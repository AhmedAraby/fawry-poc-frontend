import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceNumberPaymentResultComponent } from './reference-number-payment-result.component';

describe('ReferenceNumberPaymentResultComponent', () => {
  let component: ReferenceNumberPaymentResultComponent;
  let fixture: ComponentFixture<ReferenceNumberPaymentResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferenceNumberPaymentResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceNumberPaymentResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
