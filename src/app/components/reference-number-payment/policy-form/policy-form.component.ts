import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FawryReferenceNumberPaymentRequestModel } from 'src/app/models/fawry-reference-number-payment-request.model';
import { FawryReferenceNumberPaymentResponseModel } from 'src/app/models/fawry-reference-number-response.model';
import { ReferenceNumberPaymentService } from 'src/app/services/fawry/reference-number-payment.service';

@Component({
  selector: 'app-policy-form',
  templateUrl: './policy-form.component.html',
  styleUrls: ['./policy-form.component.css']
})
export class PolicyFormComponent implements OnInit , OnDestroy{

  paymentRefNumberSub = null;
  price: FormControl;

  constructor(private refNumPaymentService:ReferenceNumberPaymentService) { 
    this.price = new FormControl();
  }

  ngOnInit(): void {
  }

  getFawryReferenceNumber()
  {
    if(this.price.valid == false) {
      alert("enter valid number");
      return ;
    }

    // build the request
    const request: FawryReferenceNumberPaymentRequestModel = new FawryReferenceNumberPaymentRequestModel(
      "1",
      "en-gb",
       this.price.value
    );

    this.paymentRefNumberSub = this.refNumPaymentService.getFawryPaymentReferenceNumber(request).subscribe(
      (resp: FawryReferenceNumberPaymentResponseModel)=>{
        alert("reference number is : " + resp.referenceNumber);
        this.refNumPaymentService.refNumRespSubject.next(resp);
      },
      (error)=>{
        alert("failed to get payment reference number");
      }
    );

  }

  ngOnDestroy()
  {
    if(this.paymentRefNumberSub)
      this.paymentRefNumberSub.unsubscribe();
      this.refNumPaymentService.refNumRespSubject.next(null); // reset the status of the application.
    
  }
}
