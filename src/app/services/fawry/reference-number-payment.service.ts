import { Injectable } from '@angular/core';
import { FawryReferenceNumberPaymentRequestModel } from 'src/app/models/fawry-reference-number-payment-request.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { FawryReferenceNumberPaymentResponseModel } from 'src/app/models/fawry-reference-number-response.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReferenceNumberPaymentService {

  api:string = "http://localhost:8050/payment/fawry/ref";
  refNumRespSubject: BehaviorSubject<FawryReferenceNumberPaymentResponseModel | null> = new BehaviorSubject(null);
  
  constructor(private httpClient: HttpClient) { }
  getFawryPaymentReferenceNumber(request: FawryReferenceNumberPaymentRequestModel)
  {
    return this.httpClient
    .post(
      this.api, 
      request,
      {observe:"body", responseType:"json"})
      .pipe(
        map(
          (body:FawryReferenceNumberPaymentResponseModel)=>{ // body is raw js object
            return FawryReferenceNumberPaymentResponseModel.create(body);
          }
        ),
      );
    // if we need to alter the response structure here is the right place to do so 
  }
}
