import { TestBed } from '@angular/core/testing';

import { ReferenceNumberPaymentService } from './reference-number-payment.service';

describe('ReferenceNumberPaymentService', () => {
  let service: ReferenceNumberPaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReferenceNumberPaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
