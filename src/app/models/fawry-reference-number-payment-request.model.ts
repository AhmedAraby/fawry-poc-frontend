
export class FawryReferenceNumberPaymentRequestModel
{

    constructor(
        public customerId: string,
        public language: string,
        public amount: string){}

    /**
     * obj is raw js object
     */
    static create(obj: FawryReferenceNumberPaymentRequestModel)
    {
        return new FawryReferenceNumberPaymentRequestModel(
            obj['customerId'], 
            obj['language'], 
            obj['amount']
        );
    }
}