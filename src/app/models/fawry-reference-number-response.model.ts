
export class FawryReferenceNumberPaymentResponseModel
{

    constructor(
        public referenceNumber: string,
        public expiry: number){}

    /**
     * obj is raw js object
     */
    static create(obj: FawryReferenceNumberPaymentResponseModel)
    {
        return new FawryReferenceNumberPaymentResponseModel(
            obj['referenceNumber'], 
            obj['expiry'], 
        );
    }
}